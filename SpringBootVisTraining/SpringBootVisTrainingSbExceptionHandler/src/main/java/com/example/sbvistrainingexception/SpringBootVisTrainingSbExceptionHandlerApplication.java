package com.example.sbvistrainingexception;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootVisTrainingSbExceptionHandlerApplication {
	//http://localhost:8080/api/v1/todo/11
	public static void main(String[] args) {
		SpringApplication.run(SpringBootVisTrainingSbExceptionHandlerApplication.class, args);
	}

}
