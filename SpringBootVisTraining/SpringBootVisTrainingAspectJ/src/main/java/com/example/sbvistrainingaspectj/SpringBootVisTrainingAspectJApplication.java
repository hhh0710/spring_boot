package com.example.sbvistrainingaspectj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class SpringBootVisTrainingAspectJApplication {

	public static void main(String[] args) {
		 SpringApplication.run(SpringBootVisTrainingAspectJApplication.class, args);
		 //http://localhost:8080/add/employee?empId={id}&firstName={firstname}&lastName={lastname}
		 //http://localhost:8080/remove/employee?empId={id}
	}
}
