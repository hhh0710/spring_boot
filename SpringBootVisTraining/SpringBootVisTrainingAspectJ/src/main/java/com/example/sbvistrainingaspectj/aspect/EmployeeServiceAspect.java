package com.example.sbvistrainingaspectj.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class EmployeeServiceAspect {
    @Before(value = "execution(* com.example.sbvistrainingaspectj.service.EmployeeService.*(..)) && args(empId, firstName, lastName)")
    public void beforeAdvice(JoinPoint joinPoint, String empId, String firstName, String lastName) {
        System.out.println("Before method:" + joinPoint.getSignature().getName());
        System.out.println("Creating Employee with first name - " + firstName + ", second name - " + lastName + " and id - " + empId);
    }
}
