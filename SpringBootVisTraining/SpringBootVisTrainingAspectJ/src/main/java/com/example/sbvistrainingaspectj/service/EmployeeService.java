package com.example.sbvistrainingaspectj.service;

import com.example.sbvistrainingaspectj.model.Employee;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    public Employee createEmployee(String empId, String firstName, String lastName){
        Employee emp = new Employee();
        emp.setEmpId(empId);
        emp.setFirstName(firstName);
        emp.setLastName(lastName);
        return emp;
    }
    public void deleteEmployee(String empId) {

    }
}
