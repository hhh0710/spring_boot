package com.example.sbvistraininglog.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainController.class);
    @ResponseBody
    @RequestMapping(path = "/")
    public String home(){
        LOGGER.trace("This is Trace");
        LOGGER.debug("This is debug");
        LOGGER.info("This is info");
        LOGGER.warn("This is warn");
        LOGGER.error("This is error");
        return "Hí ae";
    }

}
