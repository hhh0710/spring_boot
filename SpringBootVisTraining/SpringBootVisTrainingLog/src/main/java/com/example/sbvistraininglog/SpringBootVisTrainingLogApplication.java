package com.example.sbvistraininglog;

//import com.example.sbvistraining.service.DemoService;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootVisTrainingLogApplication {
	//private static Logger logger = LoggerFactory.getLogger(SpringBootVisTrainingApplication.class);
	public static void main(String[] args) {
		ApplicationContext applicationContext = SpringApplication.run(SpringBootVisTrainingLogApplication.class, args);
		//		logger.info("debug enable: {}", logger.isDebugEnabled());
		//		logger.trace("Trace");lb
		//		logger.debug("Debug");
		//		logger.info("Info");
		//		logger.warn("Warn");
		//		logger.error("Error");

		//		DemoService demoService = applicationContext.getBean(DemoService.class);
		//		demoService.hello();
	}

}
