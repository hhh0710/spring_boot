package com.example.sbvistrainingjackson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootVisTrainingSbJacksonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootVisTrainingSbJacksonApplication.class, args);
	}

}
