package com.example.sbvistrainingjackson.annotation;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CustomDateSerializer extends JsonSerializer{

//private static final SimpleDateFormat dateFormat = new SimpleDateFormat(&quot;dd/MM/yyyy&quot;)

@Override
public void serialize(Date date, JsonGenerator generator, SerializerProvider provider)
        throws IOException, JsonProcessingException {
        String formattedDate = dateFormat.format(date);
        generator.writeString(formattedDate);
        }

    @Override
    public void serialize(Object o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

    }
}
