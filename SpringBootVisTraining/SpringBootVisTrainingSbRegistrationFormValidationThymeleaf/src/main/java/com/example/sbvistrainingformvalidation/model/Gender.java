package com.example.sbvistrainingformvalidation.model;

public class Gender {
    public static final String MALE = "Male";
    public static final String FEMALE = "Female";
}
