package com.example.sbvistrainingformvalidation.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

//Trong ví dụ này không tập trung vào vấn đề bảo mật của ứng dụng.
// Tuy nhiên chúng ta cần tới thư viện Spring Security để có thể mật mã hóa (encode) mật khẩu của người dùng trước khi lưu vào db
// Và bạn cần phải khai báo một Spring BEAN cho việc mật mã hóa mật khẩu.
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public PasswordEncoder passwordEncoder() {
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        return bCryptPasswordEncoder;
    }

    // Trong ví dụ này chúng ta không sử dụng Security.
    // Ghi đè phương thức này với Code rỗng để vô hiệu hóa Security mặc định của Spring Boot.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Empty code!
    }
}
