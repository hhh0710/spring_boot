package com.example.sbvistrainingformvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootVisTrainingSbRegistrationFormValidationThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootVisTrainingSbRegistrationFormValidationThymeleafApplication.class, args);
	}

}
