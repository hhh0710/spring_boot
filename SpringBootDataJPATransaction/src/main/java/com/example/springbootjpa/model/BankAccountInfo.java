package com.example.springbootjpa.model;

public class BankAccountInfo {
    private Long id;
    private String fulName;
    private double balance;

    public BankAccountInfo() {
    }
    // Sử dụng trong câu truy vấn của JPA
    public BankAccountInfo(Long id, String fulName, double balance) {
        this.id = id;
        this.fulName = fulName;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fulName;
    }

    public void setFulName(String fulName) {
        this.fulName = fulName;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}






