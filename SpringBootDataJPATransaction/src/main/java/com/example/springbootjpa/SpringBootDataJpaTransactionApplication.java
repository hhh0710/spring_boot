package com.example.springbootjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataJpaTransactionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataJpaTransactionApplication.class, args);
	}

}
