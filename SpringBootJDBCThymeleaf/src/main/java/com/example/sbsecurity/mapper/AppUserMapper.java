package com.example.sbsecurity.mapper;
//Lớp AppUserMapper được sử dụng để ánh xạ (mapping) các cột trong bảng APP_USER với các trường (field)
// trong lớp AppUser (Dựa trên câu lệnh truy vấn).
import java.sql.ResultSet;
import java.sql.SQLException;

import com.example.sbsecurity.model.AppUser;
import org.springframework.jdbc.core.RowMapper;

public class AppUserMapper implements RowMapper<AppUser> {

    public static final String BASE_SQL //
            = "Select u.User_Id, u.User_Name, u.Encryted_Password From App_User u ";

    @Override
    public AppUser mapRow(ResultSet rs, int rowNum) throws SQLException {

        Long userId = rs.getLong("User_Id");
        String userName = rs.getString("User_Name");
        String encrytedPassword = rs.getString("Encryted_Password");

        return new AppUser(userId, userName, encrytedPassword);
    }

}
