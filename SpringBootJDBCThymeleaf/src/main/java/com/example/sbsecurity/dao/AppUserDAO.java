package com.example.sbsecurity.dao;
//Các lớp DAO (Data Access Object) là các lớp sử dụng để truy cập vào cơ sở dữ liệu,
// chẳng hạn Query, Insert, Update, Delete. Các lớp DAO thường được chú thích bởi @Repository
// để nói với Spring rằng hãy quản lý chúng như các Spring BEAN.
//Lớp AppUserDAO sử dụng để thao tác với bảng APP_USER.
// Nó có một phương thức để tìm kiếm một người dùng trong Database ứng với tên đăng nhập nào đó.
import javax.sql.DataSource;

import com.example.sbsecurity.mapper.AppUserMapper;
import com.example.sbsecurity.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class AppUserDAO extends JdbcDaoSupport {

    @Autowired
    public AppUserDAO(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    public AppUser findUserAccount(String userName) {
        // Select .. from App_User u Where u.User_Name = ?
        String sql = AppUserMapper.BASE_SQL + " where u.User_Name = ? ";

        Object[] params = new Object[] { userName };
        AppUserMapper mapper = new AppUserMapper();
        try {
            AppUser userInfo = this.getJdbcTemplate().queryForObject(sql, params, mapper);
            return userInfo;
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

}
