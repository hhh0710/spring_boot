package com.example.sbangularjs.model;

public class Employee {
    private long id;
    private String no;
    private String name;
    private String position;

    public Employee() {
    }

    public Employee(EmployeeForm employeeForm) {
        this.id = employeeForm.getId();
        this.no = employeeForm.getNo();
        this.name = employeeForm.getName();
        this.position = employeeForm.getPosition();
    }

    public Employee(long id, String no, String name, String position) {
        this.id = id;
        this.no = no;
        this.name = name;
        this.position = position;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}
