package com.example.sbangularjs.dao;

import com.example.sbangularjs.model.Employee;
import com.example.sbangularjs.model.EmployeeForm;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class EmployeeDAO {
    private static final Map<Long, Employee> map = new HashMap<Long,Employee>();

    static {
        initEmps();
    }

    private static void initEmps(){
        Employee emp1 = new Employee(1L,"E01","Lê Văn Luyện","Hà Nội");
        Employee emp2 = new Employee(2L,"E02","Ngô Bá Khá","Bắc Ninh");
        Employee emp3 = new Employee(3L,"E03","Nguyễn Xuân Đường","Thái Bình");
        Employee emp4 = new Employee(4L,"E04","Bùi Xuân Huấn","Yên Bái");
        map.put(emp1.getId(), emp1);
        map.put(emp2.getId(), emp2);
        map.put(emp3.getId(), emp3);
        map.put(emp4.getId(), emp4);
    }
    public Long getMaxId(){
        Set<Long> keys = map.keySet();
        Long max = 0L;
        for (Long key : keys){
            if(key > max ){
                max = key;
            }
        }
        return max;
    }
    public Employee getEmployee(Long id){
        return map.get(id);
    }
    public Employee addEmployee(EmployeeForm employeeForm){
        Long id = this.getMaxId() +1;
        employeeForm.setId(id);
        Employee newEmp = new Employee(employeeForm);
        map.put(newEmp.getId(), newEmp);
        return newEmp;
    }
    public Employee updateEmployee(EmployeeForm employeeForm){
        Employee employee = this.getEmployee(employeeForm.getId());
        if(employee!= null){
            employee.setNo(employeeForm.getNo());
            employee.setName(employeeForm.getName());
            employee.setPosition(employeeForm.getPosition());
        }
        return employee;
    }
    public void deleteEmployee(Long id){
        map.remove(id);
    }
    public List<Employee> getAllEmployees(){
        Collection<Employee> collection =  map.values();
        List<Employee>  list = new ArrayList<Employee>();
        list.addAll(collection);
        return list;
    }
}















