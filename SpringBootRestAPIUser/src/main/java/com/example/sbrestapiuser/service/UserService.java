package com.example.sbrestapiuser.service;

import com.example.sbrestapiuser.model.dto.UserDto;
        import org.springframework.stereotype.Service;

        import java.util.List;

@Service
public interface UserService {
    public List<UserDto> getListUser();
    public UserDto getUserbyId(int id);
}
