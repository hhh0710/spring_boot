package com.example.sbrestapiuser.service;

        import com.example.sbrestapiuser.entity.User;
        import com.example.sbrestapiuser.model.dto.UserDto;
        import com.example.sbrestapiuser.model.mapper.UserMapper;
        import org.springframework.stereotype.Component;

        import java.util.ArrayList;
        import java.util.List;

@Component
public class UserServiceImpl implements UserService{
    private static ArrayList<User> users = new ArrayList<User>();
    static {
        users.add(new User(1,"Vung Thành Mâm","vungthanhmam1@gmail.com","0123456789","avatar.img","vtm123"));
        users.add(new User(2,"Loãn Thị Nêm","loanthinem2@gmail.com","0123456798","avatar.img","ltn321"));
        users.add(new User(3,"Nguyễn Tự Nổ","nguyentuno@gmail.com","0123456897","avatar.img","ntn213"));
        users.add(new User(4,"Nò Lựu Đạn","noluudan@gmail.com","0123456897","avatar.img","nld312"));
    }

    @Override
    public List<UserDto> getListUser() {
        List<UserDto> result = new ArrayList<UserDto>();
        for (User user : users){
            result.add(UserMapper.toUserDto(user));
        }
        return result;
    }

    @Override
    public UserDto getUserbyId(int id) {
        for (User user : users){
            if (user.getId()==id){
                return UserMapper.toUserDto(user);
            }
        }
        return null;
    }
}
