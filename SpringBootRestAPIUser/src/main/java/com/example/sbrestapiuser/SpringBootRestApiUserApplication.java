package com.example.sbrestapiuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestApiUserApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestApiUserApplication.class, args);
	}

}
